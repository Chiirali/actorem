export class ArrayUtil {
  static remove(array: Array<any>, elem: any): boolean {
    const index = array.indexOf(elem);
    if (index > -1) {
      array.splice(index, 1);
      return true;
    }
    return false;
  }
}
