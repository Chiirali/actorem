import {NgModule} from '@angular/core';
import {SimpleWebStorageModule} from '@elderbyte/ngx-simple-webstorage';
import {
  CommonDialogModule,
  ErrorHandlerModule,
  GlobalSearchModule,
  HttpSupportModule,
  LanguageModule,
  SideContentModule,
  ToastModule,
  ToolbarModule
} from '@elderbyte/ngx-starter';


@NgModule({
  imports: [
    SimpleWebStorageModule.forRoot(), ErrorHandlerModule.forRoot(),
    HttpSupportModule.forRoot(), ToolbarModule.forRoot(),
    GlobalSearchModule.forRoot(), CommonDialogModule.forRoot(),
    LanguageModule.forRoot(), SideContentModule.forRoot(),
    ToastModule.forRoot()
  ]
})
export class CoreModule {

}
