import 'hammerjs';
import {BrowserModule} from '@angular/platform-browser';
import {NgModule} from '@angular/core';
import {AppComponent} from './main/app.component';
import {appRoutes} from './app-routing.module';
import {RouterModule} from '@angular/router';
import {BrowserAnimationsModule} from '@angular/platform-browser/animations';
import {TranslateLoader, TranslateModule} from '@ngx-translate/core';
import {TranslateHttpLoader} from '@ngx-translate/http-loader';
import {SimpleWebStorageModule} from '@elderbyte/ngx-simple-webstorage';
import {HttpClient, HttpClientModule} from '@angular/common/http';
import {CoreModule} from './core/core.module';
import {MainModule} from './main/main.module';
import {LoggerFactory, LogLevel} from '@elderbyte/ts-logger';
import {environment} from '../environments/environment';
import {ActorModule} from './domain/actors/actor.module';
import {MaterialModule} from './main/material.module';
import {MovieModule} from './domain/movies/movie.module';
import {VisualisationModule} from './domain/visualisation/visualisation.module';


LoggerFactory.getDefaultConfiguration()
.withMaxLevel(environment.production ? LogLevel.Info : LogLevel.Debug);

@NgModule({
  imports: [
    BrowserModule,
    BrowserAnimationsModule,
    HttpClientModule,
    MaterialModule,
    CoreModule,
    RouterModule.forRoot(appRoutes),
    SimpleWebStorageModule.forRoot(),
    TranslateModule.forRoot({
      loader: {
        provide: TranslateLoader,
        useFactory: createTranslateLoader,
        deps: [HttpClient]
      }
    }),

    // App modules

    MainModule,
    ActorModule.forRoot(),
    MovieModule,
    VisualisationModule,
  ],
  bootstrap: [AppComponent],
  declarations: []
})
export class AppModule {
}


// Because of AOT Compiler
export function createTranslateLoader(http: HttpClient): TranslateHttpLoader {
  return new TranslateHttpLoader(http, './assets/i18n/', '.json');
}
