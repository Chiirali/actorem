import {NgModule} from '@angular/core';
import {CommonModule} from '@angular/common';
import { MatButtonModule } from '@angular/material/button';
import { MatCommonModule } from '@angular/material/core';
import { MatIconModule } from '@angular/material/icon';
import { MatListModule } from '@angular/material/list';
import { MatMenuModule } from '@angular/material/menu';
import { MatSidenavModule } from '@angular/material/sidenav';
import { MatToolbarModule } from '@angular/material/toolbar';
import {FlexLayoutModule} from '@angular/flex-layout';
import {ExpandToggleButtonModule, GlobalSearchModule, ToastModule, ToolbarModule} from '@elderbyte/ngx-starter';
import {RouterModule} from '@angular/router';
import {AppComponent} from './app.component';
import {TranslateModule} from '@ngx-translate/core';

@NgModule({
  imports: [
    CommonModule, RouterModule,

    MatCommonModule, MatSidenavModule, MatToolbarModule, MatIconModule, MatListModule,
    MatButtonModule, MatMenuModule, MatMenuModule,

    ToolbarModule, GlobalSearchModule, ToastModule, ExpandToggleButtonModule,

    FlexLayoutModule, TranslateModule,
  ],
  declarations: [AppComponent]
})
export class MainModule {


}
