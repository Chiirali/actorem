import {Component, OnInit} from '@angular/core';
import {ActivatedRouteSnapshot, NavigationEnd, Router, RouterStateSnapshot} from '@angular/router';
import {TranslateService} from '@ngx-translate/core';
import {LoggerFactory} from '@elderbyte/ts-logger';
import {filter, map} from 'rxjs/operators';


class MenuItem {

  constructor(
    public icon: string,
    public name: string,
    public link: string) {
  }
}


@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss'],
})
export class AppComponent implements OnInit {

  public searchOpen = false;
  public navigationOpen = false;
  public sideContentOpen = false;
  public menuItems: MenuItem[];
  private readonly logger = LoggerFactory.getLogger('AppComponent');

  constructor(
    private translate: TranslateService,
    private router: Router
  ) {

    this.router.events.pipe(
      filter(event => event instanceof NavigationEnd),
      map(event => event as NavigationEnd)
    ).subscribe(event => {

      if (this.isOutletActive('side')) {
        this.showSideContent();
      } else {
        this.closeSideContent();
      }

      this.closeSideNav();
    });

    translate.addLangs(['de', 'en']);

    // this language will be used as a fallback when a translation isn't found in the current language
    translate.setDefaultLang('de');

    // Set Browser Language as Init Language
    translate.use(translate.getBrowserLang());

    this.menuItems = [
      new MenuItem('view_module', 'actors.title', 'app/actors'),
      new MenuItem('view_module', 'visualisation.title', 'app/visualisation'),
    ];
  }

  ngOnInit(): void {

  }

  toggleSidenav(): void {
    this.navigationOpen = !this.navigationOpen;
  }

  closeSideNav(): void {
    this.navigationOpen = false;
  }

  public closeSideContent() {
    this.logger.trace('hiding side content ...');
    this.sideContentOpen = false;
    this.router.navigate([{outlets: {'side': null}}])
  }

  onSearchOpen(open: Event) {
    this.logger.trace('search open event', open);
    this.searchOpen = !open;
  }

  private showSideContent(): void {
    this.logger.trace('showing side content ...');
    this.sideContentOpen = true;
  }

  private isOutletActive(outlet: string): boolean {
    const rs: RouterStateSnapshot = this.router.routerState.snapshot;
    const snap: ActivatedRouteSnapshot = rs.root;
    return this.isOutletActiveRecursive(snap, outlet);
  }

  private isOutletActiveRecursive(root: ActivatedRouteSnapshot, outlet: string): boolean {

    if (root.outlet === outlet) {
      return true;
    }

    for (const c of root.children) {
      if (this.isOutletActiveRecursive(c, outlet)) {
        return true;
      }
    }
    return false;
  }
}
