import {Routes} from '@angular/router';
import {AccessDeniedComponent} from '@elderbyte/ngx-starter';
import {ActorListComponent} from './domain/actors/actor-list/actor-list.component';
import {ActorDetailSideComponent} from './domain/actors/actor-detail-side/actor-detail-side.component';
import {MovieListComponent} from './domain/movies/movie-list/movie-list.component';
import {MovieDetailSideComponent} from './domain/movies/movie-detail-side/movie-detail-side.component';
import {ActorPerformanceComponent} from './domain/visualisation/actor-performance/actor-performance.component';
import {ActorSelectionSideComponent} from './domain/visualisation/actor-selection-side/actor-selection-side.component';

export const appRoutes: Routes = [

  {
    path: '',
    redirectTo: 'app/actors',
    pathMatch: 'full',
  },

  {
    path: 'app',
    children: [
      {
        path: '',
        redirectTo: 'actors',
        pathMatch: 'full',
      },

      {
        path: '',
        redirectTo: 'movies',
        pathMatch: 'full',
      },

      {
        path: '',
        redirectTo: 'import',
        pathMatch: 'full',
      },


      {
        path: 'accessdenied',
        component: AccessDeniedComponent
      },

      // Actors
      {
        path: 'actors',
        component: ActorListComponent,
        data: {
          showGlobalSearch: false
        }
      },

      // Movies
      {
        path: 'movies',
        component: MovieListComponent,
        data: {
          showGlobalSearch: false
        }
      },

      // ActorPerformance
      {
        path: 'visualisation',
        component: ActorPerformanceComponent,
        data: {
          showGlobalSearch: false
        }
      }
    ]
  },

  /// OUTLET SIDE
  {
    path: 'actors/:actorId',
    outlet: 'side',
    component: ActorDetailSideComponent
  },

  {
    path: 'movies/:movieId',
    outlet: 'side',
    component: MovieDetailSideComponent
  },

  {
    path: 'visualisation/actor-selection',
    outlet: 'side',
    component: ActorSelectionSideComponent
  },

];

