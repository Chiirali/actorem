export class DateUtil {

  public static dateOfYear(year: number): Date {

    const month = 0;
    const day = 0;
    const hours = 0;
    const minutes = 0;
    const seconds = 0;
    const milliseconds = 0;

    return new Date(year, month, day, hours, minutes, seconds, milliseconds)
  }

}
