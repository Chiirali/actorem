import {Component, OnDestroy, OnInit} from '@angular/core';
import {ActivatedRoute, Router} from '@angular/router';


import {ToastService} from '@elderbyte/ngx-starter';
import {Movie} from '../movie';
import {MovieService} from '../movie.service';

@Component({
  selector: 'app-library-detail-side',
  templateUrl: './movie-detail-side.component.html',
  styleUrls: ['./movie-detail-side.component.scss']
})
export class MovieDetailSideComponent implements OnInit, OnDestroy {

  public movie: Movie = new Movie();

  constructor(
    private toastService: ToastService,
    private router: Router,
    private route: ActivatedRoute,
    private movieService: MovieService
  ) {
  }

  public get isModeUpdate(): boolean {
    if (this.movie) {
      return !!this.movie.id;
    }
    return false;
  }

  ngOnInit() {
    const movieId = this.route.snapshot.params['movieId'];

    if (movieId) {
      this.movieService.getById(movieId)
      .subscribe(movie => {
        this.movie = movie;
      });
    } else {
      this.movie = new Movie();
    }
  }

  ngOnDestroy(): void {
    console.warn('actor-detail is being destroyed!');
  }

  public delete(event: Event) {
    this.movieService.delete(this.movie)
    .subscribe(() => {
      this.toastService.pushInfo('Deleted movie ' + this.movie.name);
      // now close the side detail:
      this.close();
    })
  }

  public save(event: Event) {
    if (this.movie.id) {
      this.movieService.update(this.movie)
      .subscribe(movie => {
        this.toastService.pushInfo('Updated movie ' + this.movie.name);
        this.movie = movie;
        this.close();
      })
    } else {
      this.movieService.create(this.movie)
      .subscribe(movie => {
        this.toastService.pushInfo('Created movie ' + this.movie.name);
        this.movie = movie;
        this.close();
      })
    }
  }

  public close(): void {
    this.router.navigate([{outlets: {'side': []}}]);
  }

}
