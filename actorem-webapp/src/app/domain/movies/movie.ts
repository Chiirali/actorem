import {Actor} from '../actors/actor';
import {Rating} from '../ratings/rating';


export class Movie {
  id: string;
  name: string;
  year: number;
  actors: Actor[];
  rating: Rating;
}
