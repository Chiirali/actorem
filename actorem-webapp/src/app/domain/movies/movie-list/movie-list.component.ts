import {Component, OnDestroy, OnInit} from '@angular/core';

import {Router} from '@angular/router';
import {Subscription} from 'rxjs';
import {
  DataContextBuilder,
  Filter,
  GlobalSearchService,
  IDataContextContinuable,
  Sort,
  ToastService,
  ToolbarHeader,
  ToolbarService
} from '@elderbyte/ngx-starter';
import {Movie} from '../movie';
import {MovieService} from '../movie.service';
import {debounceTime} from 'rxjs/operators';


@Component({
  selector: 'app-library-list',
  templateUrl: './movie-list.component.html',
  styleUrls: ['./movie-list.component.scss']
})
export class MovieListComponent implements OnInit, OnDestroy {


  public movies: Movie[];
  public movieData: IDataContextContinuable<Movie>;
  private _subs: Subscription[] = [];

  constructor(
    private router: Router,
    private toolbarService: ToolbarService,
    private toastService: ToastService,
    private movieService: MovieService,
    private globalSearch: GlobalSearchService
  ) {
    toolbarService.title = new ToolbarHeader('movies.title');

    this.movieData = DataContextBuilder.start<Movie>()
    .buildPaged((pageable, filters) => movieService.getAll(pageable, filters));
  }

  ngOnInit() {

    this.loadMovies('');

    this._subs.push(
      this.globalSearch.queryObservable
      .pipe(
        debounceTime(200)
      )
      .subscribe(qry => {
        this.loadMovies(qry.keywords);
      }),

      this.movieService.moviesChanged
      .subscribe(() => this.loadMovies(''))
    );
  }

  ngOnDestroy(): void {
    this._subs.forEach(sub => sub.unsubscribe());
  }

  public addMovie(event: Event) {
    this.router.navigate([{outlets: {'side': ['movies', 'new']}}])
  }

  public openDetail(movie: Movie) {
    this.router.navigate([{outlets: {'side': ['movies', movie.id]}}])
  }

  private loadMovies(query: string) {
    this.movieData.start([new Sort('year', 'desc')], [new Filter('name', query)]);
  }
}
