import {Injectable} from '@angular/core';
import {environment} from '../../../environments/environment';

import {Observable, Subject} from 'rxjs';
import {Filter, HttpPagedClient, Page, Pageable} from '@elderbyte/ngx-starter';
import {HttpClient} from '@angular/common/http';
import {Movie} from './movie';
import {first, tap} from 'rxjs/operators';


@Injectable()
export class MovieService {

  /***************************************************************************
   *                                                                         *
   * Constructor                                                             *
   *                                                                         *
   **************************************************************************/

  constructor(
    private http: HttpClient,
    private httpPaged: HttpPagedClient) {
  }

  /***************************************************************************
   *                                                                         *
   * Fields                                                                  *
   *                                                                         *
   **************************************************************************/

  private _moviesChanged = new Subject();

  /***************************************************************************
   *                                                                         *
   * Public API                                                              *
   *                                                                         *
   **************************************************************************/

  public get moviesChanged(): Observable<any> {
    return this._moviesChanged;
  }

  public getAll(pageable: Pageable, filters?: Filter[]): Observable<Page<Movie>> {
    return this.httpPaged.getPaged<Movie>(environment.apiPath + 'movies', pageable, filters)
    .pipe(first());
  };

  public getById(id: string): Observable<Movie> {
    return this.http.get<Movie>(environment.apiPath + 'movies/' + id)
    .pipe(first())
  };

  public create(movie: Movie): Observable<Movie> {
    return this.http.post<Movie>(environment.apiPath + 'movies/', movie)
    .pipe(
      first(),
      tap(n => this.onChanged())
    );
  };

  public delete(movie: Movie): Observable<any> {
    return this.http.delete(environment.apiPath + 'movie/' + movie.id, {observe: 'response', responseType: 'text'})
    .pipe(
      first(),
      tap(n => this.onChanged())
    );
  };

  public update(movie: Movie): Observable<Movie> {
    return this.http.put<Movie>(environment.apiPath + 'movie/' + movie.id, movie)
    .pipe(
      first(),
      tap(n => this.onChanged())
    );
  };

  /***************************************************************************
   *                                                                         *
   * Private methods                                                         *
   *                                                                         *
   **************************************************************************/

  private onChanged() {
    this._moviesChanged.next();
  }

}
