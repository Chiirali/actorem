import {NgModule} from '@angular/core';
import {CommonModule} from '@angular/common';

import {FormsModule, ReactiveFormsModule} from '@angular/forms';
import { MatAutocompleteModule } from '@angular/material/autocomplete';
import { MatButtonModule } from '@angular/material/button';
import { MatCheckboxModule } from '@angular/material/checkbox';
import { MatCommonModule } from '@angular/material/core';
import { MatIconModule } from '@angular/material/icon';
import { MatInputModule } from '@angular/material/input';
import { MatListModule } from '@angular/material/list';
import { MatSelectModule } from '@angular/material/select';
import { MatToolbarModule } from '@angular/material/toolbar';
import {FlexLayoutModule} from '@angular/flex-layout';
import {MovieService} from './movie.service';
import {MovieListComponent} from './movie-list/movie-list.component';
import {MovieDetailSideComponent} from './movie-detail-side/movie-detail-side.component';
import {MovieDetailSheetComponent} from './movie-detail-sheet/movie-detail-sheet.component';
import {NgxChartsModule} from '@swimlane/ngx-charts';

@NgModule({
  providers: [MovieService],
  imports: [

    CommonModule, FormsModule, ReactiveFormsModule,

    MatCommonModule, MatButtonModule, MatInputModule, MatIconModule,
    MatToolbarModule, MatListModule, MatCheckboxModule, MatAutocompleteModule,
    MatSelectModule,

    NgxChartsModule,

    FlexLayoutModule
  ],
  declarations: [MovieListComponent, MovieDetailSideComponent, MovieDetailSheetComponent],
  exports: [MovieDetailSheetComponent],
  entryComponents: [MovieDetailSheetComponent]
})
export class MovieModule {
}
