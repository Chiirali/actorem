import {Component, Inject, OnInit} from '@angular/core';
import { MAT_BOTTOM_SHEET_DATA, MatBottomSheetRef } from '@angular/material/bottom-sheet';
import {MovieService} from '../movie.service';
import {LoggerFactory} from '@elderbyte/ts-logger';
import {Movie} from '../movie';

@Component({
  selector: 'app-movie-detail-sheet',
  templateUrl: './movie-detail-sheet.component.html',
  styleUrls: ['./movie-detail-sheet.component.scss']

})
export class MovieDetailSheetComponent implements OnInit {

  public movie: Movie;
  public ratingData: any[] = [];
  public view = [500, 400];
  private logger = LoggerFactory.getLogger('MovieDetailSheetComponent');

  constructor(
    private movieService: MovieService,
    private bottomSheetRef: MatBottomSheetRef<MovieDetailSheetComponent>,
    @Inject(MAT_BOTTOM_SHEET_DATA) public data: any
  ) {

  }

  ngOnInit() {

    const movieId = this.data.movieId;

    this.movieService.getById(movieId).subscribe(
      movie => {
        this.movie = movie;

        this.ratingData = [
          {
            name: 'Rating',
            value: (this.movie.rating.value * 10)
          }
        ];
      },
      err => this.logger.error('Movie not found: ' + movieId, err)
    );
  }

}
