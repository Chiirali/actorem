import {NgModule} from '@angular/core';
import {CommonModule} from '@angular/common';
import {ActorPerformanceComponent} from './actor-performance/actor-performance.component';
import {FlexLayoutModule} from '@angular/flex-layout';
import {FormsModule, ReactiveFormsModule} from '@angular/forms';
import {BrowserModule} from '@angular/platform-browser';
import {BrowserAnimationsModule} from '@angular/platform-browser/animations';
import {NgxChartsModule} from '@swimlane/ngx-charts';
import { MatAutocompleteModule } from '@angular/material/autocomplete';
import { MatBottomSheetModule } from '@angular/material/bottom-sheet';
import { MatButtonModule } from '@angular/material/button';
import { MatCheckboxModule } from '@angular/material/checkbox';
import { MatCommonModule } from '@angular/material/core';
import { MatIconModule } from '@angular/material/icon';
import { MatInputModule } from '@angular/material/input';
import { MatListModule } from '@angular/material/list';
import { MatSelectModule } from '@angular/material/select';
import { MatToolbarModule } from '@angular/material/toolbar';
import {ActorModule} from '../actors/actor.module';
import {ActorSelectionSideComponent} from './actor-selection-side/actor-selection-side.component';
import {AdvancedBubbleChartComponent} from './advanced-bubble-chart/advanced-bubble-chart.component';
import {AdvancedBubbleSeriesComponent} from './advanced-bubble-chart/advanced-bubble-series.component';

@NgModule({
  imports: [

    CommonModule, FormsModule, ReactiveFormsModule,

    MatCommonModule, MatButtonModule, MatInputModule, MatIconModule,
    MatToolbarModule, MatListModule, MatCheckboxModule, MatAutocompleteModule,
    MatSelectModule, BrowserModule, BrowserAnimationsModule,
    MatBottomSheetModule,

    NgxChartsModule,

    ActorModule, FlexLayoutModule
  ],
  declarations: [ActorPerformanceComponent, ActorSelectionSideComponent,
    AdvancedBubbleSeriesComponent,
    AdvancedBubbleChartComponent],
})
export class VisualisationModule {
}
