import {ActorFull} from '../../actors/actor';
import {Movie} from '../../movies/movie';
import {DateUtil} from '../../utils/date-util';
import {LoggerFactory} from '@elderbyte/ts-logger';

export class ChartDataBuilder {

  private static logger = LoggerFactory.getLogger('ChartDataBuilder');


  public static buildData(actor: ActorFull): any {
    return {
      name: actor.name,
      series: actor.movies.map(m => ChartDataBuilder.buildDataPoint(m))
    }

  }


  public static buildDataPoint(movie: Movie): any {
    return {
      name: movie.id,
      x: DateUtil.dateOfYear(movie.year),
      y: movie.rating.value,
      r: movie.rating.voters
    }
  }

}
