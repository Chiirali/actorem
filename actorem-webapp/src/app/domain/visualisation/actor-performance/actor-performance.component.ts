import {Component, OnDestroy, OnInit} from '@angular/core';
import {ToastService, ToolbarHeader, ToolbarService} from '@elderbyte/ngx-starter';
import {ActorFull} from '../../actors/actor';
import {ChartDataBuilder} from './chart-data-builder';
import {LoggerFactory} from '@elderbyte/ts-logger';
import {Router} from '@angular/router';
import {ActorSelectionService} from '../../actors/actor-search/actor-selection.service';
import {Subscription} from 'rxjs/internal/Subscription';
import {MovieService} from '../../movies/movie.service';
import { MatBottomSheet } from '@angular/material/bottom-sheet';
import {MovieDetailSheetComponent} from '../../movies/movie-detail-sheet/movie-detail-sheet.component';

@Component({
  selector: 'app-actor-performance',
  templateUrl: './actor-performance.component.html',
  styleUrls: ['./actor-performance.component.scss']
})
export class ActorPerformanceComponent implements OnInit, OnDestroy {

  public bubbleChartSize = [1100, 600];
  public hasData = false;
  public data: any[] = [];
  private logger = LoggerFactory.getLogger('ActorPerformanceComponent');
  private _sub: Subscription;

  constructor(
    private router: Router,
    private toolbarService: ToolbarService,
    private actorSelectionService: ActorSelectionService,
    private movieService: MovieService,
    private toastService: ToastService,
    private bottomSheet: MatBottomSheet
  ) {
    toolbarService.title = new ToolbarHeader('visualisation.title');
  }


  public ngOnInit(): void {
    this._sub = this.actorSelectionService.actorSelection.subscribe(
      actors => this.onActorsChanged(actors)
    );
  }


  ngOnDestroy(): void {
    this._sub.unsubscribe();
  }

  public onActorsChanged(newSelectedActors: ActorFull[]): void {
    this.logger.debug('Actors have changed. Updating chart now...', newSelectedActors);

    const newData = newSelectedActors.map(a => ChartDataBuilder.buildData(a));

    this.logger.debug('New Chart data:', newData);

    this.data = newData;
    this.hasData = newSelectedActors.length > 0;
  }

  public onChartSelect(event: any): void {
    const movieId = event.name.data.name;
    this.showMovieDetail(movieId);
  }

  public showActorSelection(): void {
    this.router.navigate([{outlets: {'side': ['visualisation', 'actor-selection']}}])
  }


  private showMovieDetail(movieId: string): void {


    this.bottomSheet.open(MovieDetailSheetComponent, {
      data: {
        movieId: movieId
      }
    });
  }
}
