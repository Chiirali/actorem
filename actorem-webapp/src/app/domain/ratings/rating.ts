export class Rating {

  /**
   * Rating in range [0.0 - 1.0]
   */
  public value: number;

  /**
   * Number of voters
   */
  public voters: number;
}
