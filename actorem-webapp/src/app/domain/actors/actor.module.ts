import {ModuleWithProviders, NgModule} from '@angular/core';
import {CommonModule} from '@angular/common';
import {ActorService} from './actor.service';
import {ActorListComponent} from './actor-list/actor-list.component';
import {ActorDetailSideComponent} from './actor-detail-side/actor-detail-side.component';
import {FormsModule, ReactiveFormsModule} from '@angular/forms';
import { MatAutocompleteModule } from '@angular/material/autocomplete';
import { MatButtonModule } from '@angular/material/button';
import { MatCardModule } from '@angular/material/card';
import { MatCheckboxModule } from '@angular/material/checkbox';
import { MatCommonModule } from '@angular/material/core';
import { MatIconModule } from '@angular/material/icon';
import { MatInputModule } from '@angular/material/input';
import { MatListModule } from '@angular/material/list';
import { MatSelectModule } from '@angular/material/select';
import { MatToolbarModule } from '@angular/material/toolbar';
import {FlexLayoutModule} from '@angular/flex-layout';
import {ActorSearchComponent} from './actor-search/actor-search.component';
import {ActorSelectionService} from './actor-search/actor-selection.service';

@NgModule({
  providers: [ActorService],
  imports: [

    CommonModule, FormsModule, ReactiveFormsModule,

    MatCommonModule, MatButtonModule, MatInputModule, MatIconModule,
    MatToolbarModule, MatListModule, MatCheckboxModule, MatAutocompleteModule,
    MatSelectModule, MatInputModule, MatCardModule,

    FlexLayoutModule
  ],
  declarations: [ActorListComponent, ActorDetailSideComponent, ActorSearchComponent],
  exports: [ActorSearchComponent]
})
export class ActorModule {
  static forRoot(): ModuleWithProviders {
    return {
      ngModule: ActorModule,
      providers: [
        {
          provide: ActorSelectionService,
          useClass: ActorSelectionService
        },
      ]
    };
  }
}
