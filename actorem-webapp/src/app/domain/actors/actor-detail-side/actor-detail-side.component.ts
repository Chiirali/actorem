import {Component, OnDestroy, OnInit} from '@angular/core';
import {ActivatedRoute, Router} from '@angular/router';
import {ActorService} from '../actor.service';
import {Actor} from '../actor';
import {ToastService} from '@elderbyte/ngx-starter';

@Component({
  selector: 'app-library-detail-side',
  templateUrl: './actor-detail-side.component.html',
  styleUrls: ['./actor-detail-side.component.scss']
})
export class ActorDetailSideComponent implements OnInit, OnDestroy {

  public actor: Actor = new Actor();

  constructor(
    private toastService: ToastService,
    private router: Router,
    private route: ActivatedRoute,
    private actorService: ActorService,
  ) {
  }

  public get isModeUpdate(): boolean {
    if (this.actor) {
      return !!this.actor.id;
    }
    return false;
  }

  ngOnInit() {
    const actorId = this.route.snapshot.params['actorId'];

    if (actorId) {
      this.actorService.getById(actorId)
      .subscribe(actor => {
        this.actor = actor;
      });
    } else {
      this.actor = new Actor();
    }
  }

  ngOnDestroy(): void {
    console.warn('actor-detail is being destroyed!');
  }

  public delete(event: Event) {
    this.actorService.delete(this.actor)
    .subscribe(() => {
      this.toastService.pushInfo('Deleted actor ' + this.actor.name);
      // now close the side detail:
      this.close();
    })
  }

  public save(event: Event) {
    if (this.actor.id) {
      this.actorService.update(this.actor)
      .subscribe(actor => {
        this.toastService.pushInfo('Updated actor ' + this.actor.name);
        this.actor = actor;
        this.close();
      })
    } else {
      this.actorService.create(this.actor)
      .subscribe(actor => {
        this.toastService.pushInfo('Created actor ' + this.actor.name);
        this.actor = actor;
        this.close();
      })
    }
  }

  public close(): void {
    this.router.navigate([{outlets: {'side': []}}]);
  }

}
