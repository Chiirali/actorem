import {Component, OnDestroy, OnInit} from '@angular/core';
import {ActorService} from '../actor.service';
import {Actor} from '../actor';
import {Subscription} from 'rxjs';
import {Router} from '@angular/router';

import {
  DataContextBuilder,
  Filter,
  GlobalSearchService,
  IDataContextContinuable,
  ToastService,
  ToolbarHeader,
  ToolbarService
} from '@elderbyte/ngx-starter';
import {debounceTime} from 'rxjs/operators';
import {LoggerFactory} from '@elderbyte/ts-logger';


@Component({
  selector: 'app-library-list',
  templateUrl: './actor-list.component.html',
  styleUrls: ['./actor-list.component.scss']
})
export class ActorListComponent implements OnInit, OnDestroy {

  public actorData: IDataContextContinuable<Actor>;
  private logger = LoggerFactory.getLogger('ActorListComponent');
  private _subs: Subscription[] = [];

  constructor(
    private router: Router,
    private toolbarService: ToolbarService,
    private toastService: ToastService,
    private actorService: ActorService,
    private globalSearch: GlobalSearchService
  ) {
    toolbarService.title = new ToolbarHeader('actors.title');

    this.actorData = DataContextBuilder.start<Actor>()
    .buildPaged((pageable, filters) => actorService.getAll(pageable, filters));

  }

  ngOnInit() {

    this.loadActors('');

    this._subs.push(
      this.globalSearch.queryObservable
      .pipe(debounceTime(200))
      .subscribe(qry => {
        this.loadActors(qry.keywords);
      }),

      this.actorService.actorsChanged
      .subscribe(() => this.loadActors(''))
    );
  }


  ngOnDestroy(): void {
    this._subs.forEach(sub => sub.unsubscribe());
  }

  public addActor(event: Event) {
    this.router.navigate([{outlets: {'side': ['actors', 'new']}}])
  }

  public openDetail(actor: Actor) {
    this.router.navigate([{outlets: {'side': ['actors', actor.id]}}])
  }

  private loadActors(query: string) {
    this.actorData.start([], [new Filter('name', query)])
    .subscribe(
      loaded => {
        this.logger.info('Loaded data', loaded)
      },
      err => this.logger.error('Could not load data.', err)
    );
  }
}
