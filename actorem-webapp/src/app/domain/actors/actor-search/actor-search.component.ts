import {Component, OnInit, Output} from '@angular/core';
import {Actor, ActorFull} from '../actor';
import {Observable} from 'rxjs';
import {FormControl} from '@angular/forms';
import {debounceTime, flatMap, map, startWith, tap} from 'rxjs/operators';
import {ActorService} from '../actor.service';
import {Filter, Pageable, ToastService} from '@elderbyte/ngx-starter';
import { MatAutocompleteSelectedEvent } from '@angular/material/autocomplete';
import {LoggerFactory} from '@elderbyte/ts-logger';
import {ActorSelectionService} from './actor-selection.service';

@Component({
  selector: 'app-actor-search',
  templateUrl: './actor-search.component.html',
  styleUrls: ['./actor-search.component.scss']
})
export class ActorSearchComponent implements OnInit {


  public actorSearchControl: FormControl = new FormControl();
  public actorSuggestions: Observable<Actor[]>;
  private logger = LoggerFactory.getLogger('ActorSearchComponent');

  constructor(
    private actorService: ActorService,
    private toastService: ToastService,
    private actorSelectionService: ActorSelectionService
  ) {
  }

  @Output()
  public get selectedActors(): Observable<ActorFull[]> {
    return this.actorSelectionService.actorSelection;
  }

  ngOnInit() {

    this.actorSuggestions = this.actorSearchControl.valueChanges
    .pipe(
      startWith(''),
      debounceTime(200),
      flatMap(userSearchQuery => this.actorService.getAll(
        new Pageable(0, 30),
        [new Filter('name', userSearchQuery)]
        )
      ),
      map(page => page.content),
      tap(actors => console.log('Loaded suggestions:', actors))
    );
  }

  public onOptionSelected(event: MatAutocompleteSelectedEvent): void {
    const selectedActor = event.option.value as Actor;
    if (selectedActor) {
      this.logger.info('onOptionSelected: ', selectedActor);
      this.actorService.getById(selectedActor.id).subscribe(
        fullActor => {
          this.actorSelectionService.addActor(fullActor);
          this.actorSearchControl.reset();
        },
        err => this.toastService.pushError('Could not load full actor details for actor ' + selectedActor.id, err)
      );
    }
  }

  public removeSelected(actor: ActorFull): void {
    this.actorSelectionService.removeSelected(actor);
  }


  public displayActorOption(actor: Actor): string {
    if (actor) {
      return actor.name;
    }
    return '';
  }

}
