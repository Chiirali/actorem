import {Injectable} from '@angular/core';
import {Actor, ActorFull} from '../actor';
import {BehaviorSubject, Observable} from 'rxjs';

@Injectable()
export class ActorSelectionService {

  private _currentSelection = new BehaviorSubject<ActorFull[]>([]);

  constructor() {
  }

  public get actorSelectionSnapshot(): ActorFull[] {
    return this._currentSelection.getValue();
  }

  public get actorSelection(): Observable<ActorFull[]> {
    return this._currentSelection.asObservable();
  }

  public addActor(actor: ActorFull) {
    const current = this.actorSelectionSnapshot;
    const newActors = [...current, actor];
    this.select(newActors);
  }

  public removeSelected(actor: Actor): void {
    const current = this.actorSelectionSnapshot;
    const newActors = current.filter(a => a.id !== actor.id);
    this.select(newActors);
  }

  private select(selected: ActorFull[]): void {
    this._currentSelection.next(selected);
  }
}
