import {Movie} from '../movies/movie';


export class Actor {

  public id: string;
  public name: string;
  public birthYear: number;
  public deathYear: number;

  constructor() {
  }

  public static buildDead(id: string, name: string, birthYear: number, deathYear: number): Actor {
    const actor = Actor.build(id, name, birthYear);
    actor.deathYear = deathYear;
    return actor;
  }

  public static build(id: string, name: string, birthYear: number): Actor {

    const actor = new Actor();
    actor.id = id;
    actor.name = name;
    actor.birthYear = birthYear;

    return actor;
  }

}


export class ActorFull extends Actor {

  public movies: Movie[] = [];

  constructor() {
    super();
  }

}

