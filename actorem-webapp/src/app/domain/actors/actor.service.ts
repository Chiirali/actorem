import {Injectable} from '@angular/core';
import {environment} from '../../../environments/environment';
import {Actor, ActorFull} from './actor';
import {Filter, HttpPagedClient, Page, Pageable} from '@elderbyte/ngx-starter';
import {HttpClient} from '@angular/common/http';
import {Observable, Subject} from 'rxjs';
import {first, tap} from 'rxjs/operators';


@Injectable()
export class ActorService {

  /***************************************************************************
   *                                                                         *
   * Constructor                                                             *
   *                                                                         *
   **************************************************************************/

  constructor(
    private http: HttpClient,
    private httpPaged: HttpPagedClient) {
  }

  /***************************************************************************
   *                                                                         *
   * Fields                                                                  *
   *                                                                         *
   **************************************************************************/

  private _actorsChanged = new Subject();

  /***************************************************************************
   *                                                                         *
   * Public API                                                              *
   *                                                                         *
   **************************************************************************/

  public get actorsChanged(): Observable<any> {
    return this._actorsChanged;
  }

  public getAll(pageable: Pageable, filters?: Filter[]): Observable<Page<Actor>> {
    return this.httpPaged.getPaged<Actor>(environment.apiPath + 'actors', pageable, filters)
    .pipe(first());
  };

  public getById(id: string): Observable<ActorFull> {
    return this.http.get<ActorFull>(environment.apiPath + 'actors/' + id)
    .pipe(first());
  };

  public create(actor: Actor): Observable<Actor> {
    return this.http.post<Actor>(environment.apiPath + 'actors', actor)
    .pipe(
      first(),
      tap(n => this.onChanged())
    );

  };

  public delete(actor: Actor): Observable<any> {
    return this.http.delete(environment.apiPath + 'actors/' + actor.id, {observe: 'response', responseType: 'text'})
    .pipe(
      first(),
      tap(n => this.onChanged())
    );
  };

  public update(actor: Actor): Observable<Actor> {
    return this.http.put<Actor>(environment.apiPath + 'actors/' + actor.id, actor)
    .pipe(
      first(),
      tap(n => this.onChanged())
    );
  };

  /***************************************************************************
   *                                                                         *
   * Private methods                                                         *
   *                                                                         *
   **************************************************************************/

  private onChanged() {
    this._actorsChanged.next();
  }

}
