// The environment is loaded from the backend server from /api/env.js
//
// See index.html  line: <script type="text/javascript" src="/api/env.js"></script>

export interface Environment {
  apiPath: string;
  production: string; // Careful env variables are always strings!
  issuerUrl: string;
  roleHierarchy: string;
  clientId: string;
  version: string;
}

export const environment: Environment = (window as any).env;
