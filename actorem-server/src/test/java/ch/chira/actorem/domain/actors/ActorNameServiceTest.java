package ch.chira.actorem.domain.actors;

import java.util.ArrayList;
import java.util.Collection;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

@SpringBootTest(webEnvironment = SpringBootTest.WebEnvironment.RANDOM_PORT)
@RunWith(SpringRunner.class)
public class ActorNameServiceTest {

    ActorDto actor1;
    ActorDto actor2;
    Collection<ActorDto> myMocks = new ArrayList<>();

    @Before
    public void testSetup() {
        actor1 = new ActorDto();
        actor1.id = "n1234";
        actor1.name = "Sabrönzle";
        actor1.birthYear = 1996;

        actor2 = new ActorDto();
        actor2.id = "n5657";
        actor2.name = "Gino";
        actor2.birthYear = 1960;


    }

    @Test
    public void createAll() {

        myMocks.add(actor1);
        myMocks.add(actor2);
        Assert.assertEquals(2, myMocks.size());

    }

    @Test
    public void assignValues(){
        Assert.assertEquals("n1234", actor1.id);
        Assert.assertEquals("Sabrönzle", actor1.name);

        Assert.assertEquals("n5657", actor2.id);
        Assert.assertEquals("Gino", actor2.name);


    }
}
