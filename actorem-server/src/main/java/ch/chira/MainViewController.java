package ch.chira;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;

@Controller
public class MainViewController {

    /***************************************************************************
     *                                                                         *
     * Public API                                                              *
     *                                                                         *
     **************************************************************************/

    // Support web app in sub folder
    @RequestMapping({ "/", "/actorem" })
    public String index() {
        return "forward:/actorem/index.html";
    }

    // Support deep links into web app
    @RequestMapping("/actorem/app/**")
    public String deepLinks() {
        return "forward:/actorem/index.html";
    }

}
