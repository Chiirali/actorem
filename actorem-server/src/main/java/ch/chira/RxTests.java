package ch.chira;

import io.reactivex.Flowable;

public class RxTests {

    public static void main(String[] args) {
        Flowable.just("Hello world").subscribe(System.out::println);
    }
}
