package ch.chira.actorem.mock;

import ch.chira.actorem.domain.actors.ActorDto;
import ch.chira.actorem.domain.actors.ActorFullDto;
import ch.chira.actorem.domain.actors.ActorService;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import org.springframework.beans.factory.InitializingBean;
import org.springframework.beans.factory.annotation.Autowired;


public class ActorMockDataService implements InitializingBean {

    /***************************************************************************
     *                                                                         *
     * Private Fields                                                          *
     *                                                                         *
     **************************************************************************/


    @Autowired
    private ActorService actorService;

    /***************************************************************************
     *                                                                         *
     * Public API                                                              *
     *                                                                         *
     **************************************************************************/


    @Override
    public void afterPropertiesSet() throws Exception {
        generateActorMocks();
    }

    public void generateActorMocks(){

        var actors = new ArrayList<ActorDto>();
        actors.addAll(getActorMocks());

        var actorsBulk = generate(50);

        actors.addAll(actorsBulk);

        actorService.createAll(actors);
    }

    public List<ActorFullDto> getActorMocks(){
        var actor1 = new ActorFullDto();
        actor1.id = "n1234";
        actor1.name = "Sabrönzle";
        actor1.birthYear = 1996;
        actor1.deathYear = 2007;

        var actor2 = new ActorFullDto();
        actor2.id = "franz11";
        actor2.name = "Franz";
        actor2.birthYear = 2000;
        actor2.deathYear = 1018;

        return Arrays.asList(actor1, actor2);
    }



    /***************************************************************************
     *                                                                         *
     * Private Methods                                                         *
     *                                                                         *
     **************************************************************************/


    private List<ActorFullDto> generate(int count){

        var actors = new ArrayList<ActorFullDto>();

        for(int i=0;i<count;i++){

            int start = (int)(Math.random() * 100) + 1; // [1-101]
            int age = (int)(Math.random() * 100) + 1; // [1-101]

            var actor = new ActorFullDto();
            actor.id = "generated-"+i;
            actor.name = "Foobar" + i;
            actor.birthYear = 1930+start;
            actor.deathYear =  actor.birthYear + age;

            actors.add(actor);
        }

        return actors;
    }


}
