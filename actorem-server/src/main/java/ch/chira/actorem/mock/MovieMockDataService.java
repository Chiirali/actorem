package ch.chira.actorem.mock;

import ch.chira.actorem.domain.movies.MovieDto;
import ch.chira.actorem.domain.movies.MovieService;
import ch.chira.actorem.domain.ratings.RatingDto;
import java.util.ArrayList;
import java.util.List;
import org.springframework.beans.factory.InitializingBean;
import org.springframework.beans.factory.annotation.Autowired;


public class MovieMockDataService implements InitializingBean {

    /***************************************************************************
     *                                                                         *
     * Fields                                                                  *
     *                                                                         *
     **************************************************************************/

    @Autowired
    private MovieService movieService;

    @Autowired
    private ActorMockDataService actorMockDataService;

    /***************************************************************************
     *                                                                         *
     * Public API                                                              *
     *                                                                         *
     **************************************************************************/

    @Override
    public void afterPropertiesSet() throws Exception {
        generateActorMocks();
    }

    public void generateActorMocks(){

        var movie1 = new MovieDto();
        movie1.id = "t1234";
        movie1.name = "Velo";
        movie1.year = 1940;
        movie1.rating = new RatingDto(0.4, 540);

        var movie2 = new MovieDto();
        movie2.id = "t3453";
        movie2.name = "Franz";
        movie2.year = 2016;
        movie2.rating = new RatingDto(0.3, 1400);


        var movies = new ArrayList<MovieDto>();
        movies.add(movie1);
        movies.add(movie2);

        var moviesBulk = generate(50);

        movies.addAll(moviesBulk);


        movieService.createAll(movies);
    }

    /***************************************************************************
     *                                                                         *
     * Private Methods                                                         *
     *                                                                         *
     **************************************************************************/

    private int getRandomIntNumber(int min, int max){
        return min + (int)(Math.random() * ((max - min) + 1));

    }

    private double getRandomDoubleNumber(double min, double max){
        return min + (Math.random() * ((max - min)) - 0.01);

    }

    private List<MovieDto> generate(int count){

        var movies = new ArrayList<MovieDto>();

        for(int i=0;i<count;i++){

            var movie = new MovieDto();
            movie.id = "generatedId-"+i;
            movie.name = "generatedName-"+i;
            movie.year = getRandomIntNumber(1920,2018);
            movie.rating = new RatingDto(getRandomDoubleNumber(0.0,1.0), getRandomIntNumber(4,3000)); // [0.0-1.0] / [4-3000]

            movies.add(movie);
        }

        return movies;
    }


}
