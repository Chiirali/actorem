package ch.chira.actorem.mock;

import org.springframework.boot.autoconfigure.condition.ConditionalOnProperty;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

@Configuration
@ConditionalOnProperty(value = "actorem.mockdata.enabled")
public class MockConfiguration {

    @Bean
    public ActorMockDataService actorMockDataService(){
        return new ActorMockDataService();
    }

    @Bean
    public MovieMockDataService movieMockDataService(){
        return new MovieMockDataService();
    }
}
