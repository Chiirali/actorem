package ch.chira.actorem.utils;

import io.reactivex.Observable;
import java.util.concurrent.Callable;
import java.util.stream.Stream;

public class ObsearvableStream {

    public static <T> Observable<T> fromCallable(Callable<Stream<T>> callable) {
        return Observable.create(emitter -> {
            try {
                callable.call().forEach(emitter::onNext);
                emitter.onComplete();
            } catch (Throwable t) {
                emitter.onError(t);
            }
        });
    }

    public static <T> Observable<T> fromStream(Stream<T> stream) {

        return Observable.create(emitter -> {
            try {
                stream.forEach(emitter::onNext);
                emitter.onComplete();
            } catch (Throwable t) {
                emitter.onError(t);
            }
        });
    }
}
