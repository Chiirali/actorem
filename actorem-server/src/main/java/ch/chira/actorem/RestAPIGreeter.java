package ch.chira.actorem;

import javax.annotation.security.PermitAll;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/api")
@PermitAll
public class RestAPIGreeter {

    /***************************************************************************
     *                                                                         *
     * Public API                                                              *
     *                                                                         *
     **************************************************************************/

    @GetMapping
    public String getGreeting(){
        return "Welcome to Actorem Rest API!";
    }



}
