package ch.chira.actorem.domain.parser.tsv;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;
import java.util.stream.Stream;

public class TsvData {

    /***************************************************************************
     *                                                                         *
     * Fields                                                                  *
     *                                                                         *
     **************************************************************************/

    private List<TsvDataRow> rows = new ArrayList<>();

    /***************************************************************************
     *                                                                         *
     * Constructor                                                             *
     *                                                                         *
     **************************************************************************/

    public TsvData(Collection<TsvDataRow> rows){
        this.rows.addAll(rows);
    }

    /***************************************************************************
     *                                                                         *
     * Public API                                                              *
     *                                                                         *
     **************************************************************************/

    public Stream<TsvDataRow> getRows(){
        return rows.stream();
    }

}
