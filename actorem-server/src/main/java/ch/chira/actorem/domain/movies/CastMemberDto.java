package ch.chira.actorem.domain.movies;

public class CastMemberDto {

    /***************************************************************************
     *                                                                         *
     * Fields                                                                  *
     *                                                                         *
     **************************************************************************/

    public String actorId;
    public String movieId;
    public String function;

}
