package ch.chira.actorem.domain.importer;

import ch.chira.actorem.domain.movies.MovieService;
import ch.chira.actorem.domain.parser.tsv.TsvDataRow;
import ch.chira.actorem.domain.ratings.NewRatingDto;
import io.reactivex.Observable;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class RatingImporterService {

    /***************************************************************************
     *                                                                         *
     * Private Fields                                                          *
     *                                                                         *
     **************************************************************************/

    private final Logger logger = LogManager.getLogger(getClass());

    @Autowired
    private MovieService movieService;

    /***************************************************************************
     *                                                                         *
     * Public API                                                              *
     *                                                                         *
     **************************************************************************/

    public void importAllMovieRatings(Observable<TsvDataRow> data) {
        data
            .map(this::createRatingDto)
            .buffer(2000)
            .subscribe(
                batch -> movieService.updateAndSaveRatings(batch),
                err -> logger.error("Failed to import ratings stream", err)
            );
    }

    /***************************************************************************
     *                                                                         *
     * Private Methods                                                         *
     *                                                                         *
     **************************************************************************/

    private NewRatingDto createRatingDto(TsvDataRow row) {
        var newRating = new NewRatingDto();

        newRating.movieId = row.getCellAsString("tconst")
                                .orElseThrow(() -> new IllegalStateException("No id found for rating movie row: " + row));

        newRating.value = row.getCellAsDouble("averageRating")
                                .map(imdbRating -> imdbRating / 10.0 ) // Normalize [0.0 - 1.0]
                                .orElse(0.0);


        newRating.voters = row.getCellAsInt("numVotes").orElse(0);

        return newRating;
    }


}
