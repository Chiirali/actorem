package ch.chira.actorem.domain.actors;

import ch.chira.actorem.domain.rocksdb.KeyValueStore;
import ch.chira.actorem.domain.rocksdb.RocksDbService;
import java.util.ArrayList;
import java.util.Collection;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;
import org.springframework.beans.factory.InitializingBean;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class ActorRepositoryRocks implements InitializingBean {

    @Autowired
    private RocksDbService rocksDbService;

    private KeyValueStore<ActorDto> actorDtoKeyValueStore;

    @Override
    public void afterPropertiesSet() throws Exception {
        actorDtoKeyValueStore = rocksDbService.openKeyValueStore(ActorDto.class, "actors");
    }


    public List<ActorDto> saveAll(Collection<ActorDto> actors) {
        actors.forEach(actorDto -> actorDtoKeyValueStore.put(actorDto.id, actorDto));
        return new ArrayList<>(actors);
    }

    public ActorDto findByKey(String key){
        return actorDtoKeyValueStore.get(key);
    }

    public Map<String, ActorDto> findAllByKey(Collection<String> ids) {
        return actorDtoKeyValueStore.getAll(ids);
    }

    public ActorDto save(ActorDto actorData) {
        actorDtoKeyValueStore.put(actorData.id, actorData);
        return actorData;
    }

    public Set<String> getAllIds(){
        var ids = new HashSet<String>(5000000);
        actorDtoKeyValueStore.getAllKeys()
            .subscribe(ids::add);
        return ids;
    }


}
