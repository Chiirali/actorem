package ch.chira.actorem.domain.movies;

import static java.util.stream.Collectors.toList;

import ch.chira.actorem.domain.ratings.NewRatingDto;
import ch.chira.actorem.domain.ratings.RatingDto;
import com.elderbyte.commons.utils.Stopwatch;
import java.util.Collection;
import java.util.List;
import java.util.Map;
import java.util.Objects;
import java.util.Optional;
import java.util.Set;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class MovieService {

    /***************************************************************************
     *                                                                         *
     *  Fields                                                                 *
     *                                                                         *
     **************************************************************************/

    private final Logger logger = LogManager.getLogger(getClass());


    @Autowired
    private MovieRepositoryRocks repositoryRocks;

    /***************************************************************************
     *                                                                         *
     * Constructor                                                             *
     *                                                                         *
     **************************************************************************/

    public MovieService(){

    }

    /***************************************************************************
     *                                                                         *
     * Public API                                                              *
     *                                                                         *
     **************************************************************************/

    public MovieDto create(MovieDto movieData){
        return repositoryRocks.save(movieData);
    }

    @SuppressWarnings("Duplicates")
    public List<MovieDto> createAll(Collection<MovieDto> movieDatas){

        var watch = Stopwatch.started();

        var saved = repositoryRocks.saveAll(movieDatas);

        logger.debug("Created " + saved.size() + " in " + watch.markMs() + "ms");

        return saved;
    }

    public Optional<MovieDto> findById(String id){
        return Optional.ofNullable(repositoryRocks.findByKey(id));
    }

    public List<MovieDto> updateAndSaveRatings(List<NewRatingDto> ratingsToImport) {

        var watch = Stopwatch.started();

        var movies = repositoryRocks.findAllByKey(
            ratingsToImport.stream().map(r -> r.movieId).collect(toList())
        );

        int[] events = {0,0};  // updated, skipped

        var updatedMovies = ratingsToImport.stream()
            .filter(r ->  r.value > 0.0)
            .map(r -> {
                var movie = movies.get(r.movieId);
                if(movie != null){
                    events[0]++;
                    movie.rating = new RatingDto(r.value, r.voters);
                    return movie;
                }else{
                    events[1]++;
                    return null;
                }
            })
            .filter(Objects::nonNull)
            .collect(toList());

        var saved = repositoryRocks.saveAll(updatedMovies);

        logger.debug("Updated " + events[0] + " ratings (skipped "+events[1]+") in " + watch.markMs() + "ms");

        return saved;

    }

    public Map<String, MovieDto> findAllByIds(Set<String> movieIds) {
        return repositoryRocks.findAllByKey(movieIds);
    }
}
