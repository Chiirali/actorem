package ch.chira.actorem.domain.actors;

import java.util.Objects;
import java.util.Optional;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Index;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;

/**
 * Basically a search index for actors
 */
@Entity
@Table(indexes = {
    @Index(name = "INDEX_NAME", columnList = "name"),
    @Index(name = "INDEX_BIRTH", columnList = "birthYear"),
})
public class ActorName {

    /***************************************************************************
     *                                                                         *
     * Fields                                                                  *
     *                                                                         *
     **************************************************************************/

    /*
     * An ID of an actor of the IMBD datadumps looks like this: nm1200167.
     */

    @Id
    @NotNull
    @Column(length = 15)
    private String id;

    /*
     * A name of an actor of the IMBD datadumps looks like this: Olivia Jennings.
     */

    @NotNull
    @Column(length = 150)
    private String name;

    /*
     * IMBD data dumps provide a birth year and (optional) death year, looking like 1950:
     */
    private Integer birthYear;

    /***************************************************************************
     *                                                                         *
     * Constructors                                                            *
     *                                                                         *
     **************************************************************************/

    /**
     * Empty ORM constructor.
     */
    protected ActorName(){ }

    public ActorName(String id, String name, int birthYear){
        setId(id);
        setName(name);
        setBirthYear(birthYear);
    }

    /***************************************************************************
     *                                                                         *
     * Properties                                                              *
     *                                                                         *
     **************************************************************************/

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Optional<Integer> getBirthYear() {
        return Optional.ofNullable(birthYear);
    }

    public void setBirthYear(Integer birthYear) {
        this.birthYear = birthYear;
    }

    /***************************************************************************
     *                                                                         *
     * Public API                                                              *
     *                                                                         *
     **************************************************************************/

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }
        ActorName actor = (ActorName) o;
        return Objects.equals(id, actor.id);
    }

    @Override
    public int hashCode() {

        return Objects.hash(id);
    }

}
