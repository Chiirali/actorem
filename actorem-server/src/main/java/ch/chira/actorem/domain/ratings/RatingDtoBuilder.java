package ch.chira.actorem.domain.ratings;

public class RatingDtoBuilder {

    /***************************************************************************
     *                                                                         *
     * Public API                                                              *
     *                                                                         *
     **************************************************************************/

    public static RatingDto build(Rating rating){
        var dto = new RatingDto();

        dto.value = rating.getValue();
        dto.voters = rating.getVoters();

        return dto;

    }
}
