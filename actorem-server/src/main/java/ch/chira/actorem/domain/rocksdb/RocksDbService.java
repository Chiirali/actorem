package ch.chira.actorem.domain.rocksdb;


import com.fasterxml.jackson.databind.ObjectMapper;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.Map;
import org.rocksdb.ColumnFamilyDescriptor;
import org.rocksdb.ColumnFamilyHandle;
import org.rocksdb.ColumnFamilyOptions;
import org.rocksdb.DBOptions;
import org.rocksdb.RocksDB;
import org.rocksdb.RocksDBException;
import org.springframework.beans.factory.InitializingBean;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;


@Service
public class RocksDbService implements InitializingBean {

    /***************************************************************************
     *                                                                         *
     * Fields                                                                  *
     *                                                                         *
     **************************************************************************/

    @Autowired
    private ObjectMapper mapper;

    private RocksDB rocksDB;

    private Map<String, ColumnFamilyHandle> handles = new HashMap<>();

    /***************************************************************************
     *                                                                         *
     * Public API                                                              *
     *                                                                         *
     **************************************************************************/

    @Override
    public void afterPropertiesSet() throws Exception {

        // TODO Improve this hacky init code

        try {
            var defaultOptions = new DBOptions()
                .setCreateMissingColumnFamilies(true)
                .setCreateIfMissing(true);

            var defaultDesc = new ColumnFamilyDescriptor(
                "default".getBytes(), new ColumnFamilyOptions()
            );

            var actorsDesc = new ColumnFamilyDescriptor(
                "actors".getBytes(), new ColumnFamilyOptions()
            );

            var moviesDesc = new ColumnFamilyDescriptor(
                "movies".getBytes(), new ColumnFamilyOptions()
            );

            var descriptors = Arrays.asList(defaultDesc, actorsDesc, moviesDesc);

            var handlesCb = new ArrayList<ColumnFamilyHandle>();

            rocksDB = RocksDB.open(
                defaultOptions,
                "rocksdb",
                descriptors,
                handlesCb
            );

            handles.put("default", handlesCb.get(0));
            handles.put("actors", handlesCb.get(1));
            handles.put("movies", handlesCb.get(2));


        } catch (RocksDBException e) {
            throw new IllegalStateException("Failed to open rocks-db", e);
        }
    }


    public <T> KeyValueStore<T> openKeyValueStore(Class<T> clazz, String family){

        try {
            var handle = handles.get(family);

            if(handle == null) throw new IllegalStateException("Unknown fam: " + family);

            return new RocksDbJsonKeyValueStore<T>(
                rocksDB,
                mapper,
                clazz,
                handle
            );
        } catch (Exception e) {
            throw new RuntimeException("Failed to create column family", e);
        }
    }
}
