package ch.chira.actorem.domain.actors;

import ch.chira.actorem.domain.movies.MovieDto;
import java.util.HashSet;
import java.util.Set;

public class ActorFullDto extends ActorDto {

    /***************************************************************************
     *                                                                         *
     * Fields                                                                  *
     *                                                                         *
     **************************************************************************/

    public Set<MovieDto> movies = new HashSet<>();
}
