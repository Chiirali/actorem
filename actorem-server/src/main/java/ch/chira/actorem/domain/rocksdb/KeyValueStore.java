package ch.chira.actorem.domain.rocksdb;

import io.reactivex.Observable;
import java.util.Collection;
import java.util.Map;

public interface KeyValueStore<T> {

    /***************************************************************************
     *                                                                         *
     * Fields                                                                  *
     *                                                                         *
     **************************************************************************/

    void put(String key, T value);

    T get(String key);

    Map<String, T> getAll(Collection<String> keys);

    Observable<String> getAllKeys();

}
