package ch.chira.actorem.domain.movies;


import ch.chira.actorem.domain.rocksdb.KeyValueStore;
import ch.chira.actorem.domain.rocksdb.RocksDbService;
import java.util.ArrayList;
import java.util.Collection;
import java.util.List;
import java.util.Map;
import org.springframework.beans.factory.InitializingBean;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class MovieRepositoryRocks implements InitializingBean {

    /***************************************************************************
     *                                                                         *
     * Fields                                                                  *
     *                                                                         *
     **************************************************************************/

    @Autowired
    private RocksDbService rocksDbService;

    private KeyValueStore<MovieDto> movieDtoKeyValueStore;

    /***************************************************************************
     *                                                                         *
     * Public API                                                              *
     *                                                                         *
     **************************************************************************/

    @Override
    public void afterPropertiesSet() throws Exception {
        movieDtoKeyValueStore = rocksDbService.openKeyValueStore(MovieDto.class, "movies");
    }


    public List<MovieDto> saveAll(Collection<MovieDto> movies) {
        movies
            .forEach(m -> movieDtoKeyValueStore.put(m.id, m));
        return new ArrayList<>(movies);
    }

    public MovieDto findByKey(String key){
        return movieDtoKeyValueStore.get(key);
    }

    public Map<String, MovieDto> findAllByKey(Collection<String> ids) {
        return movieDtoKeyValueStore.getAll(ids);
    }

    public MovieDto save(MovieDto movie) {
        movieDtoKeyValueStore.put(movie.id, movie);
        return movie;
    }
}
