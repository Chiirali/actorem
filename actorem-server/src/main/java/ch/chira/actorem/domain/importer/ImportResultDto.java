package ch.chira.actorem.domain.importer;

public class ImportResultDto {

    /***************************************************************************
     *                                                                         *
     * Fields                                                                  *
     *                                                                         *
     **************************************************************************/

    public long actorsImported;
    public long moviesImported;
}
