package ch.chira.actorem.domain.actors;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

@Repository
public interface ActorRepository extends JpaRepository<ActorName, String> {

    @Query("SELECT a FROM ActorName a WHERE LOWER(a.name) LIKE :nameQuery")
    Page<ActorName> findAllByNameIsLike(@Param("nameQuery") String nameQuery, Pageable pageable);

}
