package ch.chira.actorem.domain.actors;


import java.util.Collection;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/api/actors")
public class ActorResource {

    /***************************************************************************
     *                                                                         *
     * Private Fields                                                          *
     *                                                                         *
     **************************************************************************/

    @Autowired
    private ActorService actorService;

    @Autowired
    private ActorDtoBuilder actorDtoBuilder;

    /***************************************************************************
     *                                                                         *
     * Public API                                                              *
     *                                                                         *
     **************************************************************************/

    @GetMapping("/ids")
    public Collection<String> getAllIds(){
        return actorService.findAllActorIds();
    }

    @GetMapping
    public Page<ActorDto> getAll(
        @RequestParam(value = "name", required = false) String name,
        Pageable pageable){
        return actorService.findAll(name, pageable)
            .map(actorDtoBuilder::build);
    }

    @Transactional
    @GetMapping("/{actorId}")
    public ActorFullDto getActor(
        @PathVariable("actorId") String actorId){

        return actorService.findById(actorId)
            .map(actorDtoBuilder::buildFull)
            .orElseThrow(() -> new IllegalArgumentException("No actor was found with id " + actorId));
    }

}
