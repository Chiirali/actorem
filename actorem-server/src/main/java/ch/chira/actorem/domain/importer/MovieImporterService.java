package ch.chira.actorem.domain.importer;

import ch.chira.actorem.domain.movies.MovieDto;
import ch.chira.actorem.domain.movies.MovieService;
import ch.chira.actorem.domain.parser.tsv.TsvDataRow;
import io.reactivex.Observable;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class MovieImporterService {

    /***************************************************************************
     *                                                                         *
     * Private Fields                                                          *
     *                                                                         *
     **************************************************************************/

    private final Logger logger = LogManager.getLogger(getClass());

    @Autowired
    private MovieService movieService;

    /***************************************************************************
     *                                                                         *
     * Public API                                                              *
     *                                                                         *
     **************************************************************************/

    public long importAllMovies(Observable<TsvDataRow> movieTsvStream, int minReleaseYear){

        long[] total = {0}; // HACK: Enable lambda access to mutable variable

        movieTsvStream
            .filter(r -> r.getCellAsString("isAdult").orElse("0").equals("0"))
            .filter(row -> {

                var start = row.getCellAsInt("startYear").orElse(null);

                // Skip movies with no start year
                if(start == null) return false;

                // Skip movies which where released before minReleaseYear
                if(start < minReleaseYear) return false;

                return true;
            })
            .map(this::createMovieDto)
            .buffer(10000)
            .subscribe(movieBatch -> {
                    var created = movieService.createAll(movieBatch);
                    total[0] += created.size();
                },
                err -> logger.error("Failed import movies stream!", err));

        return total[0];
    }


    /***************************************************************************
     *                                                                         *
     * Private Methods                                                         *
     *                                                                         *
     **************************************************************************/




    private MovieDto createMovieDto(TsvDataRow row) {
        var movie = new MovieDto();

        movie.id = row.getCellAsString("tconst")
                                    .orElseThrow(() -> new IllegalStateException("No id found for movie row: " + row));

        movie.name = row.getCellAsString("primaryTitle")
                                    .orElseThrow(() -> new IllegalStateException("No title found for movie row: " + row));

        movie.year = row.getCellAsInt("startYear").orElse(0);

        return movie;
    }


}
