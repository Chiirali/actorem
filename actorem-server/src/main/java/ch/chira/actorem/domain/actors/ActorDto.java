package ch.chira.actorem.domain.actors;

import java.util.HashSet;
import java.util.Set;

public class ActorDto {

    /***************************************************************************
     *                                                                         *
     * Fields                                                                  *
     *                                                                         *
     **************************************************************************/

    public String id;
    public String name;
    public Integer birthYear;
    public Integer deathYear;

    public Set<String> movieIds = new HashSet<>();
}
