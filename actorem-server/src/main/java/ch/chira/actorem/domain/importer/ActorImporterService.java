package ch.chira.actorem.domain.importer;

import ch.chira.actorem.domain.actors.ActorDto;
import ch.chira.actorem.domain.actors.ActorFullDto;
import ch.chira.actorem.domain.actors.ActorService;
import ch.chira.actorem.domain.parser.tsv.TsvDataRow;
import io.reactivex.Observable;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class ActorImporterService {

    /***************************************************************************
     *                                                                         *
     * Private Fields                                                          *
     *                                                                         *
     **************************************************************************/

    private final Logger logger = LogManager.getLogger(getClass());

    @Autowired
    private ActorService actorService;



    /***************************************************************************
     *                                                                         *
     * Public API                                                              *
     *                                                                         *
     **************************************************************************/

    public long importAllActors(Observable<TsvDataRow> actorTsvStream, int birthMin){
        return importAllActors(actorTsvStream, birthMin, Integer.MAX_VALUE);
    }

    public long importAllActors(Observable<TsvDataRow> actorTsvStream, int birthMin, int birthMax){

        long[] total = {0}; // HACK: Enable lambda access to mutable variable

        actorTsvStream
            .filter(row -> {

                var birthYear = row.getCellAsInt("birthYear");
                if(!birthYear.isPresent() || birthYear.get() < birthMin || birthYear.get() > birthMax){
                    return false;
                }

                // Skip non actors
                var profession = row.getCellAsString("primaryProfession");
                if(profession.isPresent() &&
                    (profession.get().contains("actor") || profession.get().contains("actress"))){
                    return true;
                }

                return false;
            })
            .map(this::createActorDto)
            .buffer(10000)
            .subscribe(actorBatch -> {
                var created = actorService.createAll(actorBatch);
                total[0] += created;
            },
                err -> logger.error("Failed to process stream!", err));

        return total[0];
    }

    /***************************************************************************
     *                                                                         *
     * Private Methods                                                         *
     *                                                                         *
     **************************************************************************/

    private ActorDto createActorDto(TsvDataRow row) {
        var actor = new ActorFullDto();

        actor.id = row.getCellAsString("nconst")
                                    .orElseThrow(() -> new IllegalStateException("No id found for actor row: " + row));

        actor.name = row.getCellAsString("primaryName").orElse("Unknown");

        actor.birthYear = row.getCellAsInt("birthYear").orElse(0);

        actor.deathYear = row.getCellAsInt("deathYear").orElse(0);

        return actor;
    }
}
