package ch.chira.actorem.domain.importer;

import ch.chira.actorem.domain.actors.ActorService;
import ch.chira.actorem.domain.parser.tsv.TsvDataRow;
import ch.chira.actorem.domain.parser.tsv.TsvParserService;
import com.elderbyte.commons.utils.Stopwatch;
import io.reactivex.Observable;
import java.nio.file.Path;
import java.nio.file.Paths;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class ImportManagerService {

    /***************************************************************************
     *                                                                         *
     * Fields                                                                  *
     *                                                                         *
     **************************************************************************/

    private final Logger logger = LogManager.getLogger(getClass());


    @Autowired
    private ActorImporterService actorImporterService;

    @Autowired
    private MovieImporterService movieImporterService;

    @Autowired
    private RatingImporterService ratingImporterService;

    @Autowired
    private CastImporterService castImporterService;

    @Autowired
    private TsvParserService tsvParserService;

    @Autowired
    private ActorService actorService;


    /***************************************************************************
     *                                                                         *
     * Public API                                                              *
     *                                                                         *
     **************************************************************************/

    public ImportResultDto importData(Path dataFolder){
        return importData(dataFolder, Integer.MIN_VALUE, Integer.MAX_VALUE);
        // return importData(dataFolder, 1900, 2015);
    }

    public ImportResultDto importData(Path dataFolder, int birthMin, int birthMax){
        var watch = Stopwatch.started();

        dataFolder = dataFolder.toAbsolutePath();
        logger.info("Importing data from" + dataFolder);

        var result = new ImportResultDto();

        result.moviesImported = importMovies(dataFolder, birthMin);
        logger.info("Imported all raw movies within " + watch.markMs()+ " ms." );


        importRatings(dataFolder);
        logger.info("Imported all movie ratings within " + watch.markMs()+ " ms." );


        result.actorsImported = importActors(dataFolder, birthMin, birthMax);
        logger.info("Imported all actor data within " + watch.markMs()+ " ms." );

        importCast(dataFolder);
        logger.info("Imported all movie cast within " + watch.markMs()+ " ms." );

        logger.info("Imported data-set in " + watch.sinceStartMs() + "ms");

        return result;
    }



    /***************************************************************************
     *                                                                         *
     * Private Methods                                                         *
     *                                                                         *
     **************************************************************************/

    private Observable<TsvDataRow> loadTsvData(Path dataFolder, String tsvFilePath) {
        var file = dataFolder.resolve(tsvFilePath);
        return tsvParserService.parseTsvFromFile(file);
    }

    private void importCast(Path dataFolder){

        logger.info("=========== CAST IMPORT ===========");

        var watch = Stopwatch.started();

        var actorsOfInterest = actorService.findAllActorIds();

        // Import Movie Cast
        var castTsv = loadTsvData(dataFolder, "movies/title.principals.tsv");

        castImporterService.importAllMovieCastMembers(castTsv, actorsOfInterest);

        logger.info("Imported castCrews in " + watch.markMs() + "ms" );
    }


    public void importRatings(Path dataFolder){

        logger.info("=========== RATING IMPORT ===========");

        var watch = Stopwatch.started();
        // Import ratings
        var ratingTsv = loadTsvData(dataFolder, "ratings/title.ratings.tsv");
        ratingImporterService.importAllMovieRatings(ratingTsv);

        logger.info("Imported ratings in " + watch.markMs() + "ms" );
    }


    private long importMovies(Path dataFolder, int minReleaseYear){

        logger.info("=========== MOVIE IMPORT ===========");

        var watch = Stopwatch.started();
        // Import movie data
        var moviesTsv = loadTsvData(dataFolder, "movies/title.basics.tsv");
        var moviesImported = movieImporterService.importAllMovies(moviesTsv, minReleaseYear);

        logger.info("Imported " + moviesImported + " movies in " + watch.markMs() + "ms" );
        return moviesImported;
    }

    private long importActors(Path dataFolder, int birthMin, int birthMax){

        logger.info("=========== ACTOR IMPORT ===========");

        var watch = Stopwatch.started();

        var actorTsv = loadTsvData(dataFolder, "actors/name.basics.tsv");

        logger.info("Parsed actor tsv in " + watch.markMs() + "ms. Importing into database ..." );

        var actorsImported = actorImporterService.importAllActors(actorTsv, birthMin, birthMax);

        logger.info("Imported " + actorsImported + " actors in " + watch.markMs() + "ms");

        return actorsImported;
    }


}
