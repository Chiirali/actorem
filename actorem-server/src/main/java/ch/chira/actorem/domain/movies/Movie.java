package ch.chira.actorem.domain.movies;

import ch.chira.actorem.domain.ratings.Rating;
import com.elderbyte.commons.exceptions.ArgumentNullException;
import java.util.Objects;
import javax.persistence.AttributeOverride;
import javax.persistence.AttributeOverrides;
import javax.persistence.Column;
import javax.persistence.Embedded;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.validation.constraints.NotNull;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

@Entity
public class Movie {

    /***************************************************************************
     *                                                                         *
     * Fields                                                                  *
     *                                                                         *
     **************************************************************************/


    private static final Logger logger = LogManager.getLogger(Movie.class);


    /*
     * An ID of a movie of the IMBD datadumps looks like this: tt8353602.
     */

    @Id
    @NotNull
    @Column(length = 15)
    private String id;

    /*
     * A name of an movie of the IMBD datadumps looks like this: Olivia Jennings.
     */

    @NotNull
    @Column(length = 250)
    private String name;


    /*
     * Release year of the movie.
     */
    @Column
    private int year;

    @AttributeOverrides({
        @AttributeOverride(name="value",
            column=@Column(name="rating_value")),
        @AttributeOverride(name="voters",
            column=@Column(name="rating_voters"))
    })
    @NotNull
    @Embedded
    private Rating rating;


    /***************************************************************************
     *                                                                         *
     * Constructors                                                            *
     *                                                                         *
     **************************************************************************/

    /**
     * Empty ORM constructor.
     */
    protected Movie(){}

    public Movie(String id, String name, int year, Rating rating) {
        if(id == null) throw new ArgumentNullException("id");
        this.id = id;

        setName(name);
        setYear(year);
        setRating(rating);
    }


    /***************************************************************************
     *                                                                         *
     * Properties                                                              *
     *                                                                         *
     **************************************************************************/

    public String getId() {
        return id;
    }



    public String getName() {
        return name;
    }

    public void setName(String name) {

        if(name.length() > 250){
            logger.warn("Too long movie title shortened to 250 characters. Original was: " + name);
            name = name.substring(0, 248);
        }

        if(name.length() > 250){
            logger.warn("Too long movie title shortened to 250 characters. Original was: " + name);
            name = name.substring(0, 248);
        }

        this.name = name;
    }

    public int getYear() {
        return year;
    }

    public void setYear(int year) {
        this.year = year;
    }

    public Rating getRating() {
        return rating;
    }

    public void setRating(Rating rating) {
        if(rating == null) throw new ArgumentNullException("rating");
        this.rating = rating;
    }


    /***************************************************************************
     *                                                                         *
     * Public API                                                              *
     *                                                                         *
     **************************************************************************/


    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }
        Movie movie = (Movie) o;
        return Objects.equals(id, movie.id);
    }

    @Override
    public int hashCode() {

        return Objects.hash(id);
    }
}
