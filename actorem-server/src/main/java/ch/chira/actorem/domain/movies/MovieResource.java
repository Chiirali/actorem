package ch.chira.actorem.domain.movies;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/api/movies")
public class MovieResource {

    @Autowired
    private MovieService movieService;


    @GetMapping("/{movieId}")
    public MovieDto getMovie(
        @PathVariable("movieId") String movieId){
        return movieService.findById(movieId)
                                .orElseThrow(() -> new IllegalArgumentException("No movie was found with id " + movieId));
    }

}
