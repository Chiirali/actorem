package ch.chira.actorem.domain.movies;

import ch.chira.actorem.domain.ratings.RatingDtoBuilder;

public class MovieDtoBuilder {

    /***************************************************************************
     *                                                                         *
     * Public API                                                              *
     *                                                                         *
     **************************************************************************/


    public static MovieDto build(Movie movie){

        var dto = new MovieDto();

        dto.id = movie.getId();
        dto.name = movie.getName();
        dto.year = movie.getYear();
        dto.rating = RatingDtoBuilder.build(movie.getRating());

        return dto;
    }

}
