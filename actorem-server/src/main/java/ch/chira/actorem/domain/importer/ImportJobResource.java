package ch.chira.actorem.domain.importer;

import java.nio.file.Paths;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/api/importJobs")
public class ImportJobResource {

    @Autowired
    private ImportManagerService importManagerService;


    /*@PostMapping
    public ImportResultDto startImportJob(){
        return importManagerService.importData("/Users/tamara/Desktop/data/");
    }*/

    @GetMapping("/start") // Simple command to test directly in browser.
    public ImportResultDto startImportJobDemo(){
        return importManagerService.importData(Paths.get("data/"));
    }

    @GetMapping("/start/ratings") // Simple command to test directly in browser.
    public void importRatingsOnly(){
        importManagerService.importRatings(Paths.get("data/"));
    }

}
