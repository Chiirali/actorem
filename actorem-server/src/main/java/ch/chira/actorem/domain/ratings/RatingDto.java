package ch.chira.actorem.domain.ratings;

public class RatingDto {

    /***************************************************************************
     *                                                                         *
     * Fields                                                                  *
     *                                                                         *
     **************************************************************************/

    public double value;
    public int voters;

    /***************************************************************************
     *                                                                         *
     * Constructors                                                            *
     *                                                                         *
     **************************************************************************/


    public RatingDto(){}
    public RatingDto(double value, int voters){
        this.value = value;
        this.voters = voters;
    }
}
