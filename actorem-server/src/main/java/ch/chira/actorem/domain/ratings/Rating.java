package ch.chira.actorem.domain.ratings;

import javax.persistence.Column;
import javax.persistence.Embeddable;
import javax.validation.constraints.NotNull;

@Embeddable
public class Rating {

    /***************************************************************************
     *                                                                         *
     * Fields                                                                  *
     *                                                                         *
     **************************************************************************/

    /*
     * A value of a movie in the imbd data dump looks like this: 9.2.
     * The floating number goes from [0.0 - 1.0].
     */

    @NotNull
    @Column
    private double value;

    @NotNull
    @Column
    private int voters;


    /***************************************************************************
     *                                                                         *
     * Constructors                                                            *
     *                                                                         *
     **************************************************************************/

    /**
     * Empty ORM constructor.
     */
    protected Rating(){}

    public Rating(double value, int voters) {
        setValue(value);
        setVoters(voters);
    }

    /***************************************************************************
     *                                                                         *
     * Properties                                                              *
     *                                                                         *
     **************************************************************************/

    public double getValue() {
        return value;
    }

    public void setValue(double value) {
        if(value < 0.0 || value > 1.0 ) {throw new IllegalArgumentException("is not a valid rating value: " + value );}
        this.value = value;
    }

    public int getVoters() {
        return voters;
    }

    public void setVoters(int voters) {
        this.voters = voters;
    }
}
