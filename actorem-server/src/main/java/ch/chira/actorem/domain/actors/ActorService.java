package ch.chira.actorem.domain.actors;

import static java.util.stream.Collectors.toList;

import ch.chira.actorem.domain.movies.CastMemberDto;
import com.elderbyte.commons.utils.Stopwatch;
import java.util.Collection;
import java.util.Collections;
import java.util.List;
import java.util.Map;
import java.util.Optional;
import java.util.Set;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

@Service
public class ActorService {


    /***************************************************************************
     *                                                                         *
     * Private Fields                                                          *
     *                                                                         *
     **************************************************************************/

    private final Logger logger = LogManager.getLogger(getClass());


    @Autowired
    private ActorRepository repository;

    @Autowired
    private ActorRepositoryRocks rocksRepository;

    /***************************************************************************
     *                                                                         *
     * Constructor                                                             *
     *                                                                         *
     **************************************************************************/

    public ActorService(){

    }

    /***************************************************************************
     *                                                                         *
     * Public API                                                              *
     *                                                                         *
     **************************************************************************/

    public ActorDto create(ActorDto actorData){
        createAll(Collections.singletonList(actorData));
        return actorData;
    }

    @SuppressWarnings("Duplicates")
    @Transactional
    public int createAll(Collection<ActorDto> actorDatas){
        var watch = Stopwatch.started();

        updateActorIndex(actorDatas);
        var saved = rocksRepository.saveAll(actorDatas);

        var time = watch.markMs();
        double latency = (((double)time) / ((double)saved.size()));
        logger.debug("Created " + saved.size() + " actors in " + time + "ms. Avg Latency: " + latency );

        return saved.size();
    }

    public Set<String> findAllActorIds() {
        return rocksRepository.getAllIds();
    }

    @Transactional
    public Page<ActorName> findAll(String nameQuery, Pageable pageable){

        if(nameQuery != null && !nameQuery.isEmpty()){
            return repository.findAllByNameIsLike( "%" + nameQuery.toLowerCase() + "%", pageable);
        }else {
            return repository.findAll(pageable);
        }
    }

    public Optional<ActorDto> findById(String id){
        return Optional.ofNullable(rocksRepository.findByKey(id));
    }

    public Map<String, ActorDto> findByIds(Collection<String> ids){
        return rocksRepository.findAllByKey(ids);
    }

    /**
     * Update the Actors with their movies.
     * @param castMembers
     */
    public long updateCastMembers(Map<String, List<CastMemberDto>> castMembers) {


        var watch = Stopwatch.started();

        var actors = findByIds(castMembers.keySet());

        var updated = actors.values().stream()
                .map(a -> {
                    var movies = castMembers.get(a.id);
                    movies.forEach(m -> a.movieIds.add(m.movieId));
                    return a;
                }).collect(toList());

        var saved = rocksRepository.saveAll(updated).size();

        // TODO save (empty) movie to movie service

        logger.debug("Imported " + saved + " actor casts in " + watch.markMs() + "ms");

        return saved;
    }

    /***************************************************************************
     *                                                                         *
     * Private Methods                                                         *
     *                                                                         *
     **************************************************************************/

    private ActorName build(ActorDto actorData){
        return new ActorName(
            actorData.id,
            actorData.name,
            actorData.birthYear
        );
    }

    private void updateActorIndex(Collection<? extends ActorDto> actorDatas){
        var actors = actorDatas.stream()
            .map(this::build)
            .collect(toList());
        repository.saveAll(actors);
    }


}
