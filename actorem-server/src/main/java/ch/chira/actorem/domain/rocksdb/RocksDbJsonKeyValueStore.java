package ch.chira.actorem.domain.rocksdb;

import static java.util.stream.Collectors.toList;
import static java.util.stream.Collectors.toMap;

import com.fasterxml.jackson.databind.ObjectMapper;
import io.reactivex.Observable;
import java.nio.charset.Charset;
import java.util.Collection;
import java.util.Map;
import org.rocksdb.ColumnFamilyHandle;
import org.rocksdb.RocksDB;
import org.rocksdb.RocksDBException;
import org.rocksdb.RocksIterator;

/**
 * Implements a JSON based key value storage on top of rocks db.
 * @param <T>
 */
public class RocksDbJsonKeyValueStore<T> implements KeyValueStore<T> {

    /***************************************************************************
     *                                                                         *
     * Fields                                                                  *
     *                                                                         *
     **************************************************************************/

    private static Charset charset = Charset.forName("UTF-8");
    private final RocksDB rocksDB;
    private final ObjectMapper mapper;
    private final Class<T> jsonClazz;
    private final ColumnFamilyHandle columnFamily;

    /***************************************************************************
     *                                                                         *
     * Constructor                                                             *
     *                                                                         *
     **************************************************************************/

    /**
     * Creates a new Rocks DB Json Adapter
     * @param rocksDB The rocks db
     * @param mapper A jackson json mapper
     * @param jsonClazz The DTO which represents the JSON object.
     * @param columnFamily The rocks db column
     */
    RocksDbJsonKeyValueStore(
        RocksDB rocksDB,
        ObjectMapper mapper,
        Class<T> jsonClazz,
        ColumnFamilyHandle columnFamily
    ) {
        this.mapper = mapper;
        this.rocksDB = rocksDB;
        this.jsonClazz = jsonClazz;
        this.columnFamily = columnFamily;
    }

    /***************************************************************************
     *                                                                         *
     * Public API                                                              *
     *                                                                         *
     **************************************************************************/

    @Override
    public void put(String key, T value) {
        try {
            var keyBytes = toKeyBytes(key);
            var valueBytes = mapper.writeValueAsBytes(value);
            rocksDB.put(columnFamily, keyBytes, valueBytes);
        } catch (Exception e) {
            throw new KeyStoreException("Failed to store KV! key: " + key, e);
        }
    }

    @Override
    public T get(String key) {
        try {
            var bytes = rocksDB.get(columnFamily, toKeyBytes(key));
            return convert(bytes);
        } catch (Exception e) {
            throw new RuntimeException("Failed to read key " + key, e);
        }
    }

    @Override
    public Map<String, T> getAll(Collection<String> keys) {

        try {
            var byteKeys = keys.stream()
                .map(RocksDbJsonKeyValueStore::toKeyBytes)
                .collect(toList());

            var families = keys.stream()
                .map(k -> columnFamily)
                .collect(toList());

            var bytePairs = rocksDB.multiGet(families, byteKeys);

            var pairs = bytePairs.entrySet().stream()
                .collect(
                    toMap(
                        es -> toKey(es.getKey()),
                        es -> convert(es.getValue()))
                );



            return pairs;

        } catch (RocksDBException e) {
            throw new RuntimeException("Failed to get all!", e);
        }
    }

    @Override
    public Observable<String> getAllKeys(){
        return streamKeys(rocksDB.newIterator(columnFamily))
            .map(this::toKey);
    }

    /***************************************************************************
     *                                                                         *
     * Private Methods                                                         *
     *                                                                         *
     **************************************************************************/

    private static byte[] toKeyBytes(String key){
        return key.getBytes(charset);
    }

    private String toKey(byte[] keyBytes){
        return new String(keyBytes, charset);
    }

    private static Observable<byte[]> streamKeys(RocksIterator it) {
        return Observable.create(emitter -> {
            try {
                for (it.seekToFirst(); it.isValid(); it.next()) {
                    emitter.onNext(it.key());
                }
                emitter.onComplete();
            } catch (Throwable t) {
                emitter.onError(t);
            }
        });
    }

    private T convert(byte[] valueBytes){
        try {
            return mapper.readValue(valueBytes, jsonClazz);
        } catch (Exception e) {
            throw new RuntimeException("Failed to deserialize json value " + valueBytes, e);
        }
    }
}
