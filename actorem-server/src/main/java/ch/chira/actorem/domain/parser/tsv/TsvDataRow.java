package ch.chira.actorem.domain.parser.tsv;

import java.util.Arrays;
import java.util.Optional;

public class TsvDataRow {

    /***************************************************************************
     *                                                                         *
     * Fields                                                                  *
     *                                                                         *
     **************************************************************************/

    private final TsvHeaderRow header;
    private final String[] values;
    private final int rowNr;

    /***************************************************************************
     *                                                                         *
     * Constructor                                                             *
     *                                                                         *
     **************************************************************************/

    public TsvDataRow(String[] values, TsvHeaderRow header, int rowNr){
        this.header = header;
        this.values = values;
        this.rowNr = rowNr;
    }

    /***************************************************************************
     *                                                                         *
     * Public API                                                              *
     *                                                                         *
     **************************************************************************/

    public Optional<String> getCellAsString(String columnName) {
        return header.getIndexForColumnName(columnName)
                        .map(index -> values[index]);
    }

    public Optional<Integer> getCellAsInt(String columnName) {
        return getCellAsString(columnName)
                        .filter(strValue -> !strValue.equals("\\N"))
                        .map(strValue -> {
                            try {
                                return Integer.parseInt(strValue);
                            }catch (Exception e){
                                throw new RuntimeException("Could not parse number: " + strValue);
                            }
                        });
    }

    public Optional<Double> getCellAsDouble(String columnName) {
        return getCellAsString(columnName)
            .filter(strValue -> !strValue.equals("\\N"))
            .map(strValue -> Double.parseDouble(strValue));

    }

    @Override
    public String toString() {
        return "TsvDataRow{" +
            "header=" + header +
            ", values=" + (values == null ? null : Arrays.asList(values)) +
            ", rowNr=" + rowNr +
            '}';
    }


}
