package ch.chira.actorem.domain.parser.tsv;

import ch.chira.actorem.utils.ObsearvableStream;
import com.elderbyte.commons.utils.Stopwatch;
import io.reactivex.Observable;
import java.nio.file.Files;
import java.nio.file.Path;
import java.util.stream.Stream;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.stereotype.Service;


@Service
public class TsvParserService {

    /***************************************************************************
     *                                                                         *
     * Fields                                                                  *
     *                                                                         *
     **************************************************************************/

    private final Logger logger = LogManager.getLogger(getClass());


    /***************************************************************************
     *                                                                         *
     * Public API                                                              *
     *                                                                         *
     **************************************************************************/


    public Observable<TsvDataRow> parseTsvFromFile(Path filePath){

        logger.info("Parsing tsv rows from " + filePath);
        var watch = Stopwatch.started();

        var header = getHeader(filePath);

        return observableFromLines(filePath)
            .skip(1)
            .map(row -> buildTsvRow(row, header, 0));
    }

    /***************************************************************************
     *                                                                         *
     * Private Methods                                                         *
     *                                                                         *
     **************************************************************************/

    private TsvHeaderRow getHeader(Path filePath){
        var headerRow = streamLines(filePath).findFirst().get();
        return buildHeader(headerRow);
    }

    /**
     * Build a TsvRow from a raw string like:
     *
     * Example data:
     * nm0000002	Lauren Bacall	1924	2014	actress,soundtrack	tt0038355,tt0037382,tt0117057,tt0040506
     *
     * @param rawRow
     */
    private TsvDataRow buildTsvRow(String rawRow, TsvHeaderRow header, int rowNr) {
        var rawCellValues = rawRow.split("\t");
        return new TsvDataRow(rawCellValues, header, rowNr);
    }


    /**
     * Build the tsv header row model.
     *
     * Example data:
     * nconst	primaryName	birthYear	deathYear	primaryProfession	knownForTitles
     *
     * @param headerRow
     */
    private TsvHeaderRow buildHeader(String headerRow){
        var columnNames = headerRow.split("\t");
        return TsvHeaderRow.fromColumns(columnNames);
    }

    private Observable<String> observableFromLines(Path filePath){
        return ObsearvableStream.fromStream(streamLines(filePath));
    }

    private Stream<String> streamLines(Path filePath){
        try {
            return Files.lines(filePath);
        }catch (Exception e){
            throw new RuntimeException(e);
        }
    }
}
