package ch.chira.actorem.domain.importer;

import static java.util.stream.Collectors.groupingBy;

import ch.chira.actorem.domain.actors.ActorService;
import ch.chira.actorem.domain.movies.CastMemberDto;
import ch.chira.actorem.domain.parser.tsv.TsvDataRow;
import io.reactivex.Observable;
import java.util.Collection;
import java.util.List;
import java.util.Map;
import java.util.Set;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class CastImporterService {

    /***************************************************************************
     *                                                                         *
     * Private Fields                                                          *
     *                                                                         *
     **************************************************************************/

    private final Logger logger = LogManager.getLogger(getClass());

    @Autowired
    private ActorService actorService;

    /***************************************************************************
     *                                                                         *
     * Public API                                                              *
     *                                                                         *
     **************************************************************************/

    public long importAllMovieCastMembers(Observable<TsvDataRow> data, Set<String> actorsOfInterest) {

        long total[] = {0,0};

        data

            .filter(row -> {
                // Skip all non actors
                var cat = row.getCellAsString("category").orElse(null);

                if( cat == null ){ return false; }

                if(!(cat.equals("actor") || cat.equals("actress"))){
                    return false; // No actor
                }

                // Skip all actors of no interest
                var rowActorId = row.getCellAsString("nconst").orElse(null);
                return rowActorId != null && actorsOfInterest.contains(rowActorId);
            })
            .buffer(5000)
            .map(this::buildCastMembers)
            .subscribe(castMembers -> {
                var importedCast = actorService.updateCastMembers(castMembers);
                total[0] += importedCast;
            },
                err -> logger.error("Failed to import movie cast stream!", err));


        return total[0];
    }



    /***************************************************************************
     *                                                                         *
     * Private Methods                                                         *
     *                                                                         *
     **************************************************************************/

    private Map<String, List<CastMemberDto>> buildCastMembers(Collection<TsvDataRow> data) {
        return data.stream()
            .map(this::createCastMemberDto)
            .collect(groupingBy(cm -> cm.actorId));
    }


    private CastMemberDto createCastMemberDto(TsvDataRow row) {
        var castMembers = new CastMemberDto();

        castMembers.movieId = row.getCellAsString("tconst")
            .orElseThrow(() -> new IllegalStateException("No movie-id found " + row));

        castMembers.actorId = row.getCellAsString("nconst")
            .orElseThrow(() -> new IllegalStateException("No actor-id found " + row));

        return castMembers;
    }



}
