package ch.chira.actorem.domain.parser.tsv;

import static java.util.stream.Collectors.toList;

import java.util.HashMap;
import java.util.Map;
import java.util.Optional;

/**
 * Represents column schema containing column name mapped to the index.
 */
public class TsvHeaderRow {

    /***************************************************************************
     *                                                                         *
     * Static Builders                                                         *
     *                                                                         *
     **************************************************************************/

    public static TsvHeaderRow fromColumns(String[] columns){
        return new TsvHeaderRow(buildHeader(columns));
    }

    /***************************************************************************
     *                                                                         *
     * Fields                                                                  *
     *                                                                         *
     **************************************************************************/

    private final Map<String, Integer> header;

    /***************************************************************************
     *                                                                         *
     * Constructors                                                            *
     *                                                                         *
     **************************************************************************/

    private TsvHeaderRow(Map<String, Integer> header){
        this.header = header;
    }

    /***************************************************************************
     *                                                                         *
     * Public API                                                              *
     *                                                                         *
     **************************************************************************/

    /**
     * Look up index for a given column name.
     * In case column name doesn't exist, an empty Optional is returned.
     *
     * @param columnName the name of the column
     * @return the index of the column
     */
    public Optional<Integer> getIndexForColumnName(String columnName) {
        return Optional.ofNullable(header.get(columnName));
    }

    @Override
    public String toString() {

        var lines = header.entrySet().stream()
                            .map(e -> e.getKey() + " @ " + e.getValue())
                            .collect(toList());

        return "TsvHeaderRow{" +
            String.join("\n", lines) +
            '}';
    }

    /***************************************************************************
     *                                                                         *
     * Private Methods                                                         *
     *                                                                         *
     **************************************************************************/

    private static Map<String, Integer> buildHeader(String[] columns){
        var header = new HashMap<String, Integer>();
        for (int i = 0; i < columns.length; i++) {
            header.put(columns[i], i);
        }
        return header;
    }


}
