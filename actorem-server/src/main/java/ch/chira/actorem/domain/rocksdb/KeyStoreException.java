package ch.chira.actorem.domain.rocksdb;

public class KeyStoreException extends RuntimeException {
    public KeyStoreException(String message, Exception cause){
        super(message, cause);
    }
}
