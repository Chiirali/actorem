package ch.chira.actorem.domain.actors;

import ch.chira.actorem.domain.movies.MovieService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class ActorDtoBuilder {

    @Autowired
    private MovieService movieService;


    /***************************************************************************
     *                                                                         *
     * Public API                                                              *
     *                                                                         *
     **************************************************************************/

    public ActorDto build(ActorName actor){

        var dto = new ActorDto();
        updateDto(dto, actor);
        return dto;
    }


    public ActorFullDto buildFull(ActorDto actor){
        var dto = new ActorFullDto();
        dto.id = actor.id;
        dto.name = actor.name;
        dto.birthYear = actor.birthYear;
        dto.deathYear = actor.deathYear;

        dto.movies.addAll(
            movieService.findAllByIds(actor.movieIds).values()
        );

        return dto;
    }


    private void updateDto(ActorDto dto, ActorName actor){
        dto.id = actor.getId();
        dto.name = actor.getName();
        dto.birthYear = actor.getBirthYear().orElse(null);
    }

}
