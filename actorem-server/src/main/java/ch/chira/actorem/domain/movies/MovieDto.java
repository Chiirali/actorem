package ch.chira.actorem.domain.movies;

import ch.chira.actorem.domain.ratings.RatingDto;


public class MovieDto {

    /***************************************************************************
     *                                                                         *
     * Fields                                                                  *
     *                                                                         *
     **************************************************************************/


    public String id;
    public String name;
    public int year;
    public RatingDto rating = new RatingDto();

    public MovieDto(){}
    public MovieDto(String id){this.id = id;}

}
