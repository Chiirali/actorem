
![](https://i.imgur.com/tnFj8sD.png)

Actorem is a fashionable web-app that enables to combine different data dumps from IMDB to generate powerful visualisations.

The main goal is to display, if actors /actresses tend to get in better movies over time in their career. If the movie was "good" is defined by the IMDB rating. The more voters the significanter is the bubble in the visualisation.

## Preview

![](https://i.imgur.com/qPsF4PS.png)

## Visualisation

Sketch of the Visualisation page

![](https://i.imgur.com/C3uw9K9.png)

##Data Amount

Data amount was by far the biggest challenge in this project. All four sets combined are bigger than 2 GB text data.
Additionally joining the data sets was also a big part of the challenge.
 Regarding data handling I learnt alot. The data import is reduced to overall 10 minutes for ca. 10 Mio data records.

## IMBD Data Sets

| Name                    | Content                | Size                                                                                                                                       |
|-------------------------|------------------------|-------------------
| name.basics.tsv.gz      | Contains the person per id, their birth and optional death year, and which movie per id they are most known for. | 536 MB|
| title.basics.tsv.gz     | Contains the movie per id, the movie's name. | 434.8 MB|
| title.ratings.tsv.gz    | Contains the movie per id, their imbd rating value and how many voters voted on the rating. | 5.314 B                                        |
| title.principals.tsv.gz | Contains the movie title with an identifier and the most credited principal with an identifier such as an actor.                                   | 1,2 GB|


###Database Solution


| Database | Usage                                                        |
|----------|------------------------------------------------------------------|
| RocksDB  | Key Value Store which holds completely all data from my project. |
| H2       | Relational database which holds only actors name and id. The name is indexed. |
|          |                                                                  |


The tsv files are parsed, then streamed the database in **batches** to import (each time 10'000 elements). Batches are possible because I work with Observables. All the data is stored in RocksDB, which is a key value store.
While importing the Actors, it stores them as well in a H2 database.

I indexed this database for the name.
Therefore, I can implement searching for an actor's name in the frontend.

###Rest API
Basically, I send only the whole actors to the fronted.
The actors are paged, so that the performance is improved.


Only the id of the movies are sent along.
Later on when I need them, I fetch and laod them from RocksDB.

###Import Process

1. import Movies
2. import Ratings
3. update Movies
4. import Actors (store them in both Dbs and index in h2)
5. import cast crew
6. update actors with movie ids.


## Technology stack

* The backend is written with Java 10, Spring Boot 2, JPA / Hibernate
* Supporting currently H2 / PostgresSQL and RocksDB
* The frontend is written with Angular 6 and Angular Material 5 and Typescript.
* The visualisations are made with the @swimlane/ngx-charts libray which is based on D3.

## Running the application

1. First you need to import it in your IDEA by Gradle.
2. It will ask you to configure angular cli, click ok.
3. Indexing takes a bit of time.
4. Afterwards go to: `cd actorem-webapp/` and install the dependencies by  `npm install`
5. Running the frontend: `npm run start` in actorem-webapp/.
6. Running the backend: `gradle bootRun` in the source folder.
7. http://localhost:4200/actorem/app/visualisation
8. (Rest API on http://localhost:8080/api)

