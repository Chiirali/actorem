## Docker image which includes data

### How to build
1. Prepare build
  * Copy the latest data set in this folder as `data`
  * Login to the private gitlab registry `docker login registry.gitlab.com`
  * Pull latest actorem image `docker pull registry.gitlab.com/chiirali/actorem:latest`
2. Build the image including data
  * Build the docker image `docker build . -t registry.gitlab.com/chiirali/actorem/actorem-data:latest`
  * Push image to registry `docker push registry.gitlab.com/chiirali/actorem/actorem-data:latest`

### Run locally

1. `docker run -p 8080:80 registry.gitlab.com/chiirali/actorem/actorem-data:latest`
2. Go to http://localhost:8080
3. You might want to kick off the import `http://localhost:8080/api/importJobs/start`
 * This will take some time. A decent machine might take up to 5 min